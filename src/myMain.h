#pragma once

#include <QObject>
#include <QCoreApplication>
#include <QTextStream>
#include <QVector>
#include <chicken.h>

class MyMain : public QObject
{
    Q_OBJECT
private:
    QCoreApplication *app;
    QVector<Chicken*> chickens;
    QVector<QThread*> threads;

    void startChicken();
    void newChicken();
    void getStatus();
    void stopChickens();

    char getUserInput();
    void printMenu();

public:
    explicit MyMain(QObject *parent = 0);

    void endWork();

signals:
    void startNewChickenSignal();
    void stopLayingSignal();
    void finishWork();
    void finished();

public slots:
    void run();
    void aboutToQuitApp();
};