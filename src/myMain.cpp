#include <random>
#include <QObject>
#include <QCoreApplication>
#include <QTextStream>
#include <QVector>
#include <QThread>

#include <myMain.h>
#include <chicken.h>

QTextStream cin(stdin);
QTextStream cout(stdout);


MyMain::MyMain(QObject *parent) :
    QObject(parent)
{
    app = QCoreApplication::instance();

    QVector<Chicken*> chickens;
    QVector<QThread*> threads;
}

void MyMain::run()
{
    bool exit = false;

	cout << "Chicken farm..." << endl;

	do
	{
		char opt = getUserInput();
		cout << endl;

		switch(opt)
		{
			case '1':
				newChicken();
				break;
			case '2':
				getStatus();
				break;
			case '3':
				stopChickens();
				break;
			case '4':
				exit = true;
                stopChickens();                
				break;								
		}
	}while(!exit);

    endWork();
}

void MyMain::endWork()
{
    bool allStopped;

    cout << "Wait for all chickens to finish..." << endl;

    do 
    {
        allStopped = true;
        for(Chicken* x: chickens)
	    {
            allStopped &= x->IsStopped();
        }
    } while(!allStopped);

    cout << " done." << endl;

    getStatus();

    emit finished();
}

void MyMain::aboutToQuitApp()
{
	for(Chicken* x: chickens)
	{
		delete x;
	}
	cout << "Chickens deleted." << endl;
}

void MyMain::newChicken()
{
	std::random_device generator;
	std::uniform_int_distribution<int> distribution(2,10);

	Chicken* x = new Chicken(distribution(generator));
	chickens.push_back(x);

    QThread* myThread = new QThread;
    threads.push_back(myThread);

    connect(this, 
            SIGNAL(startNewChickenSignal()),
            x,
            SLOT(Start())
            );

    connect(this, 
            SIGNAL(stopLayingSignal()),
            x,
            SLOT(Stop())
            );

/*
    connect(x, 
            SIGNAL(stopped()),
            myThread,
            SLOT(terminate())
            );
*/

    x->moveToThread(myThread);
    myThread->start();

    emit startNewChickenSignal();

}

void MyMain::getStatus()
{
	int count = 0;
	for(Chicken* x: chickens)
	{
		cout <<	QString("%1. chicken (%2s) %3 %4 egg%5")
			.arg(++count, 3, 10, QChar(' ')).rightJustified(3)
			.arg(x->GetLayingInterval(), 2, 10, QChar(' '))
			.arg((x->IsStopped() ? " (stopped): " : ": "), 12, QChar(' '))
			.arg(x->GetNumberOfEggs(), 3, 10, QChar(' '))
			.arg((x->GetNumberOfEggs() > 1 ? "s.":".")) << endl;
	}
}

void MyMain::stopChickens()
{
    emit stopLayingSignal();
    
	cout << "Chickens signed to stop laying." << endl;
}

void MyMain::printMenu()
{
	cout << endl;
	cout << "Select one option:" << endl;
	cout << "1 - Spawn a new chicken" << endl;
	cout << "2 - Get chickens status " << endl;
	cout << "3 - Stop chickens" << endl;
	cout << "4 - Quit" << endl;
}

char MyMain::getUserInput()
{
	char c;
	do
	{
		printMenu();
		cin >> c;
	} while((c > '4') || (c < '1'));

	return c;
}