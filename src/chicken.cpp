#include <QTextStream>
#include <QThread>
#include <chicken.h>

QTextStream ccout(stdout);

Chicken::Chicken(int interval)
	: layingInterval(interval),
      laying(true),
      stop(false),
      eggs(0)
{
    ccout << "New chicken with " << interval << "s interval" << endl;
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Chicken::Laying);
    timer->setSingleShot(true);
}

void Chicken::Start()
{
    timer->start(layingInterval * 1000);
}

void Chicken::Laying()
{
    eggs++;

    if (laying = !stop)
    {
        Start();
    }
    else
    {
        emit stopped();
    }
    
}

void Chicken::Stop()
{
    stop = true;
}

int Chicken::GetNumberOfEggs()
{
    return eggs;
}

int Chicken::GetLayingInterval()
{
    return layingInterval;
}

bool Chicken::IsStopped()
{
    return !laying;
}

