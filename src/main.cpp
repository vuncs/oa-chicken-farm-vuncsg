#include <QCoreApplication>
#include <QTimer>

#include <myMain.h>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    MyMain myMain;

    QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
    QObject::connect(&app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));

    QTimer::singleShot(10, &myMain, SLOT(run()));
    return app.exec();
}