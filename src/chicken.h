#pragma once

#include <QObject>
#include <QTimer>

class Chicken: public QObject
{
    Q_OBJECT
private:
    int eggs;
    int layingInterval;
    bool stop;
    bool laying;
    QTimer *timer;

public:    
    Chicken(int interval);

    void Laying();
    int GetNumberOfEggs();
    int GetLayingInterval();    
    bool IsStopped();

signals:
    void stopped();

public slots:
    void Start();
    void Stop(); 
    
};
